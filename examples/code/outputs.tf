output "backend_bucket_id" {
  description = "Id of the created bucket s3"
  value       = module.module_usage.backend_bucket_id
}

output "backend_bucket_arn" {
  description = "arn of the created bucket s3"
  value       = module.module_usage.backend_bucket_arn
}

output "backend_bucket_name" {
  description = "name of the created bucket s3"
  value       = module.module_usage.backend_bucket_name
}

output "backend_table_name" {
  description = "name of the storage table"
  value       = module.module_usage.backend_table_name
}

output "backend_configuration_file_dot_env" {
  description = "generate the configuration file for the backend infrastructure"
  value       = module.module_usage.backend_configuration_file_dot_env
}

output "project_backend_configuration_file_dot_env" {
  description = "generate the project configuration file for use remote backend state"
  value       = module.module_usage.project_backend_configuration_file_dot_env
}

output "project_configuration_file_dot_env" {
  description = "generate the project configuration file .env"
  value       = module.module_usage.project_configuration_file_dot_env
}

output "debug" {
  description = "For debug purpose."
  value       = module.module_usage.debug
}
